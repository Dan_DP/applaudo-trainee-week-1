

const form = document.querySelector('#sign-in-form form');
const levelOfExperienceText = document.querySelector('#level-of-experience-text');
levelOfExperienceText.innerHTML = 50;

let user = {
    name: document.getElementById('name'),
    lastname: document.getElementById('lastname'),
    email: document.getElementById('email'),
    password: document.getElementById('password'),
    phone: document.getElementById('phone'),
    age: document.getElementById('age'),
    gender: '',
    website: document.getElementById('website'),
    levelOfExperience: document.getElementById('level-of-experience'),
    errors: {
        name: document.getElementById('error-name'),
        lastname: document.getElementById('error-lastname'),
        email: document.getElementById('error-email'),
        password: document.getElementById('error-password'),
        confirmPassword: document.getElementById('error-confirm-password'),
        phone: document.getElementById('error-phone'),
        age: document.getElementById('error-age'),
        gender: document.getElementById('error-gender'),
        website: document.getElementById('error-website'),
        terms: document.getElementById('error-terms'),
    }
};
const radiosGender = document.getElementsByName('gender');
const agreedTerms = document.getElementById('terms');

const clearErrors = (errors) => {
    for(let errorName in errors){
        errors[errorName].innerText = '';
    }
}

const formValidation = ({name,lastname,email,password,phone,age,website,errors}) => {
    clearErrors(errors);

    let nameRegex = /^[^\d|!"#$%&/()=?¡°'¿´+}¨*\]{[,.\-;:_/<>]+$/;
    //name
    if(!name.value.trim()) errors.name.innerText='Name is required';
    else{
        if(nameRegex.exec(name.value.trim())===null) errors.name.innerText = 'Invalid name';
    }
    //lastname
    if(!lastname.value.trim()) errors.lastname.innerText='Lastname is required';
    else{
        if(nameRegex.exec(lastname.value.trim())===null) errors.lastname.innerText = 'Invalid lastname';
    }
    //email
    if(!email.value.trim()) errors.email.innerText='Email is required';
    else{
        let emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(emailRegex.exec(email.value.trim())===null) errors.email.innerText = 'Email format is invalid';
    }
    //password
    if(!password.value.trim()) errors.password.innerText='Password is required';
    else{
        if(!document.getElementById('confirm-password').value.trim()) errors.confirmPassword.innerText='Please confirm your password';
        else if(document.getElementById('confirm-password').value.trim() !== password.value.trim()) errors.confirmPassword.innerText='Password does not match';
    }
    //phone
    if(!phone.value.trim()) errors.phone.innerText='Phone number is required';
    else{
        let phoneRegex = /^\d{4}-\d{4}$/;
        if(phoneRegex.exec(phone.value.trim())===null) errors.phone.innerText = 'Phone format is incorrect';
    }
    //age
    if(!age.value.trim()) errors.age.innerText='Age is required';
    else{
        if(isNaN(age.value.trim()) || (parseFloat(age.value.trim())%1!==0)) errors.age.innerText='Age value is not correct';
    }
    //gender
    radiosGender.forEach(radio =>{if(radio.checked) user.gender = radio.value});
    if(user.gender==='') errors.gender.innerText='Gender is required';
    //website
    if(!website.value.trim()) errors.website.innerText='Website is required';
    else{
        if(!isValidUrl(website.value.trim())) errors.website.innerText='Website url is not valid';
    }
    
    //Terms and conditions
    if(!agreedTerms.checked) errors.terms.innerText='Please accept our tearms and conditions';

    //Final validation
    let valid = true;
    for(let errorName in errors){
        if(errors[errorName].innerText !== '') valid = false;
    }
    return valid;
}

const isValidUrl = (url) => {
    if (url.indexOf('https://')==-1 && url.indexOf('http://')==-1) {
        url = 'http://' + url
    };			
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name and extension
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?'+ // port
      '(\\/[-a-z\\d%@_.~+&:]*)*'+ // path
      '(\\?[;&a-z\\d%@_.,~+&:=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    if(!pattern.test(url)) {
        return false		
    } else {        
        return true;
    }
}

form.addEventListener('submit',(e) => {
    e.preventDefault();
    
    if(formValidation(user)){
        let validatedUser = {};
        for(let prop in user){
            if(prop === 'errors') continue;
            validatedUser[prop] = (prop!=='gender')?user[prop].value:user[prop];
            switch(prop){
                case 'age':
                case 'levelOfExperience':
                    validatedUser[prop] = parseFloat(validatedUser[prop]);
                    break;
            }
        }
        console.log(validatedUser);
    }
});

user.levelOfExperience.addEventListener('input',() => {
    levelOfExperienceText.innerHTML = user.levelOfExperience.value;
})

//Creating Applaudo secret code behaviour
const secretModalDiv = document.getElementById('footer');
let secretCode = '';
let isSecretModalVisible = false;

window.onload = () => {
    setInterval(()=>secretCode = '',100000);
};

window.addEventListener('keyup',e => {
    secretCode += e.key;
    switch(e.key){
        case 'F5':
        case 'Escape':
            secretCode = '';
            break;
    }
    if(secretCode.toLowerCase()==='applaudo2020' && !isSecretModalVisible) createSecretModal();
});

const createSecretModal = () => {
    isSecretModalVisible = true;

    //Creating base elements
    let modalDiv = document.createElement('div');
    modalDiv.classList.add('modal');
    modalDiv.style.display = "block";

    let contentDiv = document.createElement('div');
    contentDiv.classList.add('modal-content');

    let closeSpan = document.createElement('span');
    closeSpan.classList.add('close');
    closeSpan.textContent = 'X';

    let secretTextDiv = document.createElement('div');
    secretTextDiv.classList.add('secret-text');

    let secretTextH1 = document.createElement('h1');
    secretTextH1.textContent = 'Admins working... zzz';

    let secretImageDiv = document.createElement('div');
    secretImageDiv.classList.add('secret-image');

    let secretImageElement = document.createElement('img');
    secretImageElement.src = 'img/secret.gif';

    //Wrapping all together
    secretImageDiv.appendChild(secretImageElement);
    secretTextDiv.appendChild(secretTextH1);
    contentDiv.appendChild(closeSpan);
    contentDiv.appendChild(secretTextDiv);
    contentDiv.appendChild(secretImageDiv);
    modalDiv.appendChild(contentDiv);
    secretModalDiv.appendChild(modalDiv);

    closeSpan.addEventListener('click',() => {
        modalDiv.style.display = "none";
        secretModalDiv.innerHTML = '';
        isSecretModalVisible = false;
        secretCode = '';
    });
}
